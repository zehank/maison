from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from users.forms import UserRegisterForm, UserLoginForm  # UserUpdateForm #, ProfileUpdateForm
from django.contrib.auth import authenticate, login, logout


def register(request):
    context = {}
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            user= form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            account = authenticate(email=email, password=raw_password)
            login(request, account)
            messages.success(request, f'Account was created for {user}!')
            return redirect('login')
        else:
            form = UserRegisterForm()

    else:
        form = UserRegisterForm()
    return render(request, 'registration.html', {'form':form})


def logout_view(request):
    logout(request)
    return redirect('logout')


def loginpage(request):
    if request.method =='POST':
        email= request.POST.get('email')
        password= request.POST.get('password')
        user = authenticate(request, email=email, password=password)

        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.info(request,'Email or Password is incorrect')
    form = UserLoginForm()
    context = {
        'form': form
    }
    return render(request,'login.html',context)


# @login_required
# def profile(request):
#     if request.method == 'POST':
#         u_form = UserUpdateForm(request.POST, instance=request.user)
#         p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
#         if u_form.is_valid() and p_form.is_valid():
#             u_form.save()
#             p_form.save()
#             messages.success(request, f'Profile Updated')
#             return redirect('profile')
#     else:
#         u_form = UserUpdateForm(instance=request.user)
#         p_form = ProfileUpdateForm(instance=request.user.profile)
#
#     context = {
#         'u_form': u_form,
#         'p_form': p_form
#     }
#     return render(request, 'users/profile.html', context)
