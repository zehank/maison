from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from django.contrib.auth import authenticate

from users.models import RegisterUser


def password_match(password, c_password):
    return password == c_password


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField(max_length=60, widget=forms.EmailInput(
        attrs={'class': 'form-control custom_input', 'placeholder':'Enter Email'}),
                            help_text='Required.Add a valid email address')
    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={'class': 'form-control custom_input','name':'password1', 'placeholder': 'Enter Password'}))
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={'class': 'form-control custom_input','name': 'password2', 'placeholder': 'Confirm Password'}))

    class Meta:
        model = RegisterUser
        fields = ['f_name', 'l_name', 'username', 'email', 'password1', 'password2', 'credit_card']
        widgets = {
            'f_name': forms.TextInput(attrs={'class': 'form-control custom_input', 'placeholder': 'First Name'}),
            'l_name': forms.TextInput(attrs={'class': 'form-control custom_input', 'placeholder': 'Last Name'}),
            'username': forms.TextInput(attrs={'class': 'form-control custom_input', 'placeholder': 'User Name'}),
            # 'password1': forms.PasswordInput(
            #     attrs={'class': 'form-control custom_input','name': 'password1', 'placeholder': 'Enter Password'}),
            # 'password2': forms.PasswordInput(
            #     attrs={'class': 'form-control custom_input', 'name': 'password2', 'placeholder': 'Confirm Password'}),
            'credit_card': forms.TextInput(
                attrs={'class': 'form-control custom_input', 'placeholder': 'Enter Credit Card Number'}),
        }

    def clean(self):
        cleaned_data = super(UserRegisterForm, self).clean()
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')

        if password1 and password2:
            if not password_match(password1, password2):
                raise forms.ValidationError('Passwords do not match.')
        return cleaned_data


class UserLoginForm(forms.ModelForm):
    password = forms.TextInput()

    class Meta:
        model = RegisterUser
        fields = ['email', 'password']
        widgets = {
            'email': forms.EmailInput(attrs={'class': 'form-control custom_input','name': 'email', 'placeholder': 'Enter Email'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control custom_input','name': 'password', 'placeholder': 'Enter Password'})
        }

    def clean(self):
        if self.is_valid():
            email = self.cleaned_data['email']
            password = self.cleaned_data['password']
            if not authenticate(email=email, password=password):
                raise forms.ValidationError("Invalid login")

# class AccountAuthenticationForm(forms.ModelForm):
#
# 	password = forms.CharField(label='Password', widget=forms.PasswordInput)
#
# 	class Meta:
# 		model = Account
# 		fields = ('email', 'password')
#
# 	def clean(self):
# 		if self.is_valid():
# 			email = self.cleaned_data['email']
# 			password = self.cleaned_data['password']
# 			if not authenticate(email=email, password=password):
# 				raise forms.ValidationError("Invalid login")
# class UserUpdateForm(forms.ModelForm):
#     email = forms.EmailField()
#
#     class Meta:
#         model = User
#         fields = ['username', 'email']

# class ProfileUpdateForm(forms.ModelForm):
#     class Meta:
#         model = Profile
#         fields = ['image']

# class RegiForm(forms.Form):
#     GENDER_CHOICES = [
#         ('MALE', 'Male'),
#         ('FEMALE', 'Female'),
#         ]
#     f_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control custom_input','placeholder': 'First Name'}))
#     l_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control custom_input','placeholder': 'Last Name'}))
#     u_name = forms.SlugField(widget=forms.TextInput(attrs={'class':'form-control custom_input','placeholder': 'User Name'}))
#     email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control custom_input','placeholder': 'info@example.com'}))
#     phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Enter Phone Number', 'type': 'phone', 'class': 'form-control custom_input'}))
#     address = forms.CharField(widget=forms.Textarea(attrs={'placeholder':'Enter Address', 'class': 'form-control custom_input'}))
#     credit_card = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Enter Credit Card Number', 'class': 'form-control custom_input'}))
#     password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Enter Password', 'class': 'form-control custom_input'}))
#     confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password', 'class': 'form-control custom_input'}))
#     gender = forms.ChoiceField(choices=GENDER_CHOICES, widget=forms.Select(attrs={'class':'form-control custom_input','default': 'Gender'}))
