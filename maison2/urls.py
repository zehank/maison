from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path,include
from users import views as users_views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('admin/', admin.site.urls),
    path('registration/', users_views.register, name='register'),
    # path('registration/',users_views.regui, name='registration'),
    # path('profile/',users_views.profile, name='profile'),
    # path('login/',auth_views.LoginView.as_view(template_name='login.html'),name='login'),
    path('login/', users_views.loginpage, name='login'),
    path('logout/',auth_views.LogoutView.as_view(template_name='logout.html'),name='logout'),
    path('', include('maison.urls')),

]
